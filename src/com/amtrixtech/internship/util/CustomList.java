/*
    DO NOT MODIFY THIS FILE. HOWEVER, YOU CAN COMPLETE insert(int data, int index) METHOD
    IF YOU WANT. IT WILL BE A PLUS POINT FOR YOU. YOU CAN FIND THAT METHOD AT THE BOTTOM OF
    THIS FILE.
 */

package com.amtrixtech.internship.util;

public class CustomList {

    /**************************************************/
    /********************* DO NOT MODIFY *************/
    /**************************************************/

    private int size = 0;

    private Node headNode = null;

    private Node tailNode = null;

    public Node getHeadNode() { return headNode; }

    public static class Node {
        private int data;
        private Node nextNode;

        public Node(int data) {
            this.data = data;
            this.nextNode = null;
        }

        public int getData() { return data; }

        public Node getNextNode() { return nextNode; }

        public void setData(int data) { this.data = data; }

        public void setNextNode(Node node) {
            this.nextNode = node;
        }
    }

    public int getSize() { return size; }

    private void create(int data) {
        Node newNode = new Node(data);
        headNode = newNode;
        tailNode = headNode;
        size = 1;
    }

    public void insert(int data) {
        if (getSize() == 0) {
            create(data);
        } else {
            Node newNode = new Node(data);
            tailNode.setNextNode(newNode);
            tailNode = newNode;
            size++;
        }
    }

    public int remove(int index) {
        if (index < 0 || index >= getSize()) {
            throw new RuntimeException("IndexOutOfBoundsException for index: " + index);
        }
        if (index == 0) {
            size--;
            headNode = headNode.getNextNode();
        }
        Node head = headNode.getNextNode();
        int cursor = 1;
        int data = -1;
        Node currentNode = headNode;
        while(head != null && cursor < index) {
            cursor++;
            currentNode = head;
            head = head.getNextNode();
        }
        if (cursor == index) {
            data = head.getData();
            if (index == size - 1) {
                tailNode = currentNode;
            }
            currentNode.setNextNode(head.getNextNode());
            size--;
        }
        return data;
    }

    @Override
    public String toString() {
        if (getSize() == 0) return "[]";
        StringBuilder sb = new StringBuilder();
        Node head = headNode;
        sb.append("[ ");
        while(head != null) {
            sb.append(head.getData() + ",");
            head = head.getNextNode();
        }
        sb.append(" ]");
        return sb.toString();
    }

    /************************************************************/
    /******************* YOU CAN MODIFY FROM HERE *************/
    /************************************************************/

    public void insert(int data, int index) {
        // here, you have to insert the data in the given index
        // please note that the index starts from zero
        // your code goes here
             
        Node newNode = new Node(data);
        // if new node is to be added at the first position
        
        if(index == 0){         
            newNode.setNextNode(headNode);
            headNode = newNode;    
            size++;        
        }
        // if new node isto be added at last position 
        else if(index == getSize()){
            tailNode.setNextNode(newNode);
            tailNode = newNode;
            size++;

        }
        else {
            // if new node isto be added at middle of the list 
            Node temp1 = getHeadNode();
            
            for(int i = 0;i<getSize();i++)
            {   

               if(i==index-1){
                newNode.nextNode=temp1.nextNode;
                temp1.nextNode = newNode;
                temp1 = newNode;
                size++;
               }
               else{
                temp1 = temp1.nextNode;
               }
            }

        }

    

        

    }

}
