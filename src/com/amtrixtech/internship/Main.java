/*
    DO NOT MODIFY THIS FILE. HOWEVER, IF YOU HAVE ATTEMPTED CustomList.java,
    YOU CAN UNCOMMENT THE LAST LINE OF MAIN METHOD.
 */

package com.amtrixtech.internship;

import com.amtrixtech.internship.task.MainTask;
import com.amtrixtech.internship.util.CustomList;

public class Main {

    private static MainTask mainTask = new MainTask();

    private static CustomList populateList1() {
        CustomList list = new CustomList();

        list.insert(10);
        list.insert(20);
        list.insert(15);
        list.insert(100);
        list.insert(-10);
        list.insert(19);
        list.insert(34);
        list.insert(80);

        return list;
    }

    private static CustomList populateList2() {
        CustomList list = new CustomList();

        list.insert(10);
        list.insert(20);
        list.insert(30);
        list.insert(40);
        list.insert(50);
        

        return list;
    }

    public static void test1() {
        CustomList list = populateList1();
        System.out.println("List: " + list.toString());
        System.out.println("Max Value: " + mainTask.getMaxValue(list.getHeadNode())); // 100
        System.out.println("Average Value: " + mainTask.getAverageValue(list.getHeadNode())); // 33.0
    }

    public static void test2() {
        CustomList list = populateList2();
        System.out.println("List: " + list.toString());
        System.out.println("Max Value: " + mainTask.getMaxValue(list.getHeadNode())); // 50
        System.out.println("Average Value: " + mainTask.getAverageValue(list.getHeadNode())); // 30.0
    }

    public static void main(String[] args) {
        test1();
        test2();
       customTest(); // uncomment this method to test your insert(data, index) method
    }

    public static void customTest() {
        CustomList list = new CustomList();
        list.insert(10);
        list.insert(20);
        list.insert(30);
        list.insert(40);
        list.remove(2); // 30 is removed
        list.insert(25, 2); // 25 should be added in between 20 and 40
        list.insert(28, 3); // 28 should be added in between 25 and 40
        System.out.println(list.toString()); // the list should be [ 10,20,25,28,40, ]
        list.remove(3); // 28 should be removed
        list.remove(3); // 40 should be removed
        System.out.println("Size of list: " + list.getSize()); // 3 should be the size
        System.out.println(list.toString()); // the list should be [ 10,20,25, ]
    }
}
