package com.amtrixtech.internship.task;

import static com.amtrixtech.internship.util.CustomList.Node;

public class MainTask {

    /*
        METHODS AVAILABLE FOR Node:
            int getData() : returns the data present in the node. The data has int datatype.
            Node getNextNode() : returns the reference of the next node
            void setNextNode(Node newNode) : sets the next node reference of current node to the newNode
     */

    /*
        Here, you're be given a reference to the head of a list
        Your task is to find the maximum value present in the entire list.
     */
    public int getMaxValue(Node headNode) {
        
        int max = headNode.getData();
        Node temp = headNode.getNextNode();
        // loop run until the last item of list is not reached 
       while(temp!=null){
        if(max < temp.getData()){
            max = temp.getData();
            temp = temp.getNextNode();
            
        }
        else
        {
            temp = temp.getNextNode();
        }
       }

        return max;
    }

    /*
        Here, you're given a reference to the head of a list
        Your task is to find the average value from entire list
     */
    public double getAverageValue(Node headNode) {
        double sum ;
        // count is set to one because the temp is pointing towards second item
        int count = 1;
         sum = headNode.getData();
        Node temp = headNode.getNextNode();
       while(temp!=null){
        sum = sum+temp.getData();
        temp = temp.getNextNode();
        count ++;
       }
        return (sum/count);
    }

}
